/* spiderweb-application.c
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "spiderweb-application.h"
#include "spiderweb-preferences.h"
#include "spiderweb-window.h"

struct _SpiderwebApplication
{
  AdwApplication parent_instance;
  WebKitSettings *web_settings;
  WebKitWebContext *web_context;
  GSettings *settings;
  guint enable_javascript : 1;
};

G_DEFINE_TYPE (SpiderwebApplication, spiderweb_application, ADW_TYPE_APPLICATION)

enum {
  PROP_0,
  PROP_ENABLE_JAVASCRIPT,
  N_PROPS
};

static GParamSpec *properties [N_PROPS];

SpiderwebApplication *
spiderweb_application_new (const char        *application_id,
                           GApplicationFlags  flags)
{
  return g_object_new (SPIDERWEB_TYPE_APPLICATION,
                       "application-id", application_id,
                       "flags", flags,
                       NULL);
}

static void
spiderweb_application_finalize (GObject *object)
{
  SpiderwebApplication *self = SPIDERWEB_APPLICATION (object);

  g_clear_object (&self->settings);

  G_OBJECT_CLASS (spiderweb_application_parent_class)->finalize (object);
}

static void
spiderweb_application_get_property (GObject    *object,
                                    guint       prop_id,
                                    GValue     *value,
                                    GParamSpec *pspec)
{
  SpiderwebApplication *self = SPIDERWEB_APPLICATION (object);

  switch (prop_id)
    {
    case PROP_ENABLE_JAVASCRIPT:
      g_value_set_boolean (value, spiderweb_application_get_enable_javascript (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiderweb_application_set_property (GObject      *object,
                                    guint         prop_id,
                                    const GValue *value,
                                    GParamSpec   *pspec)
{
  SpiderwebApplication *self = SPIDERWEB_APPLICATION (object);

  switch (prop_id)
    {
    case PROP_ENABLE_JAVASCRIPT:
      spiderweb_application_set_enable_javascript (self, g_value_get_boolean (value));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiderweb_application_activate (GApplication *app)
{
  GtkWindow *window;

  g_assert (GTK_IS_APPLICATION (app));

  window = gtk_application_get_active_window (GTK_APPLICATION (app));

  if (window == NULL)
    window = g_object_new (SPIDERWEB_TYPE_WINDOW,
                           "application", app,
                           NULL);

  gtk_window_present (window);
}

static void
about_action (GSimpleAction *action,
              GVariant      *params,
              gpointer       user_data)
{
  static const gchar *authors[] = {"Christian Hergert", NULL};
  SpiderwebApplication *self = SPIDERWEB_APPLICATION (user_data);
  GtkWindow *window = NULL;

  g_return_if_fail (SPIDERWEB_IS_APPLICATION (self));

  window = gtk_application_get_active_window (GTK_APPLICATION (self));

  gtk_show_about_dialog (window,
                         "program-name", "spiderweb",
                         "authors", authors,
                         "version", "0.1.0",
                         NULL);
}

static void
preferences_action (GSimpleAction *action,
                    GVariant      *params,
                    gpointer       user_data)
{
  SpiderwebApplication *self = user_data;
  SpiderwebPreferences *window;

  g_assert (SPIDERWEB_IS_APPLICATION (self));

  window = spiderweb_preferences_new (self);

  gtk_window_present (GTK_WINDOW (window));
}

static void
quit_action (GSimpleAction *action,
             GVariant      *params,
             gpointer       user_data)
{
  g_application_quit (user_data);
}

static void
spiderweb_application_startup (GApplication *application)
{
  SpiderwebApplication *self = SPIDERWEB_APPLICATION (application);

  G_APPLICATION_CLASS (spiderweb_application_parent_class)->startup (application);

  self->web_context = webkit_web_context_new ();
  webkit_web_context_set_sandbox_enabled (self->web_context, TRUE);

  self->web_settings = webkit_settings_new ();
  g_object_bind_property (self, "enable-javascript",
                          self->web_settings, "enable-javascript",
                          G_BINDING_SYNC_CREATE);

  self->settings = g_settings_new ("me.hergert.Spiderweb");
  g_settings_bind (self->settings, "enable-javascript",
                   self, "enable-javascript",
                   G_SETTINGS_BIND_DEFAULT);

}

static void
spiderweb_application_class_init (SpiderwebApplicationClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->finalize = spiderweb_application_finalize;
  object_class->get_property = spiderweb_application_get_property;
  object_class->set_property = spiderweb_application_set_property;

  app_class->activate = spiderweb_application_activate;
  app_class->startup = spiderweb_application_startup;

  properties [PROP_ENABLE_JAVASCRIPT] =
    g_param_spec_boolean ("enable-javascript", NULL, NULL,
                          FALSE,
                          (G_PARAM_READWRITE | G_PARAM_EXPLICIT_NOTIFY | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);
}

static void
spiderweb_application_init (SpiderwebApplication *self)
{
  static const GActionEntry actions[] = {
   { "about", about_action },
   { "preferences", preferences_action },
   { "quit", quit_action },
  };

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   actions,
                                   G_N_ELEMENTS (actions),
                                   self);

  gtk_application_set_accels_for_action (GTK_APPLICATION (self),
                                         "app.quit",
                                         (const char * const[]) { "<Control>q", NULL, });
}

gboolean
spiderweb_application_get_enable_javascript (SpiderwebApplication *self)
{
  g_return_val_if_fail (SPIDERWEB_IS_APPLICATION (self), FALSE);

  return self->enable_javascript;
}

void
spiderweb_application_set_enable_javascript (SpiderwebApplication *self,
                                             gboolean              enable_javascript)
{
  g_return_if_fail (SPIDERWEB_IS_APPLICATION (self));

  enable_javascript = !!enable_javascript;

  if (enable_javascript != self->enable_javascript)
    {
      self->enable_javascript = enable_javascript;
      g_object_notify_by_pspec (G_OBJECT (self), properties[PROP_ENABLE_JAVASCRIPT]);
    }
}

WebKitWebContext *
spiderweb_application_get_web_context (SpiderwebApplication *self)
{
  g_return_val_if_fail (SPIDERWEB_IS_APPLICATION (self), NULL);

  return self->web_context;
}

WebKitSettings *
spiderweb_application_get_web_settings (SpiderwebApplication *self)
{
  g_return_val_if_fail (SPIDERWEB_IS_APPLICATION (self), NULL);

  return self->web_settings;
}
