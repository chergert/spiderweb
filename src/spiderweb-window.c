/* spiderweb-window.c
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "spiderweb-window.h"
#include "spiderweb-tab.h"

struct _SpiderwebWindow
{
  AdwApplicationWindow  parent_instance;

  AdwHeaderBar         *header_bar;
  AdwTabView           *tab_view;
  GtkEntry             *url_entry;

  GBindingGroup        *tab_bindings;
};

G_DEFINE_TYPE (SpiderwebWindow, spiderweb_window, ADW_TYPE_APPLICATION_WINDOW)

static void
go_backward_action (GSimpleAction *action,
                    GVariant      *param,
                    gpointer       user_data)
{
  SpiderwebWindow *self = user_data;
  SpiderwebTab *tab;

  g_assert (SPIDERWEB_IS_WINDOW (self));

  if ((tab = spiderweb_window_get_selected_tab (self)))
    spiderweb_tab_go_previous (tab);
}

static void
go_forward_action (GSimpleAction *action,
                   GVariant      *param,
                   gpointer       user_data)
{
  SpiderwebWindow *self = user_data;
  SpiderwebTab *tab;

  g_assert (SPIDERWEB_IS_WINDOW (self));

  if ((tab = spiderweb_window_get_selected_tab (self)))
    spiderweb_tab_go_next (tab);
}

static const GActionEntry history_actions[] = {
  { "go-forward", go_forward_action },
  { "go-backward", go_backward_action },
};

static void
on_url_entry_activate_cb (SpiderwebWindow *self,
                          GtkEntry        *entry)
{
  SpiderwebTab *tab;
  const char *text;

  g_assert (SPIDERWEB_IS_WINDOW (self));
  g_assert (GTK_IS_ENTRY (entry));

  text = gtk_editable_get_text (GTK_EDITABLE (entry));
  if (text == NULL || text[0] == 0)
    return;

  if (!(tab = spiderweb_window_get_selected_tab (self)))
    {
      tab = spiderweb_tab_new ();
      spiderweb_window_append_tab (self, tab);
    }

  spiderweb_tab_navigate_to (tab, text);
  gtk_widget_grab_focus (GTK_WIDGET (tab));
}

static void
new_tab_action (GtkWidget  *widget,
                const char *action_name,
                GVariant   *param)
{
  SpiderwebWindow *self = SPIDERWEB_WINDOW (widget);
  SpiderwebTab *tab;

  g_assert (SPIDERWEB_IS_WINDOW (self));

  tab = spiderweb_tab_new ();
  spiderweb_window_append_tab (self, tab);
  spiderweb_window_set_selected_tab (self, tab);
}

static void
close_tab_action (GtkWidget  *widget,
                  const char *action_name,
                  GVariant   *param)
{
  SpiderwebWindow *self = SPIDERWEB_WINDOW (widget);
  SpiderwebTab *tab;

  g_assert (SPIDERWEB_IS_WINDOW (self));

  if ((tab = spiderweb_window_get_selected_tab (self)))
    spiderweb_window_close_tab (self, tab);
  else
    gtk_window_close (GTK_WINDOW (self));
}

static void
focus_url_action (GtkWidget  *widget,
                  const char *action_name,
                  GVariant   *param)
{
  SpiderwebWindow *self = SPIDERWEB_WINDOW (widget);

  g_assert (SPIDERWEB_IS_WINDOW (self));

  gtk_widget_grab_focus (GTK_WIDGET (self->url_entry));
  gtk_editable_select_region (GTK_EDITABLE (self->url_entry), 0, -1);
}

static gboolean
tab_page_to_tab (GBinding     *binding,
                 const GValue *from_value,
                 GValue       *to_value,
                 gpointer      user_data)
{
  AdwTabPage *page = g_value_get_object (from_value);
  if (page != NULL)
    g_value_set_object (to_value, adw_tab_page_get_child (page));
  return TRUE;
}

static void
spiderweb_window_constructed (GObject *object)
{
  SpiderwebWindow *self = (SpiderwebWindow *)object;

  G_OBJECT_CLASS (spiderweb_window_parent_class)->constructed (object);

  spiderweb_window_append_tab (self, spiderweb_tab_new ());
}

static void
spiderweb_window_dispose (GObject *object)
{
  SpiderwebWindow *self = (SpiderwebWindow *)object;

  g_clear_object (&self->tab_bindings);

  G_OBJECT_CLASS (spiderweb_window_parent_class)->dispose (object);
}

static void
spiderweb_window_class_init (SpiderwebWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  object_class->constructed = spiderweb_window_constructed;
  object_class->dispose = spiderweb_window_dispose;

  gtk_widget_class_set_template_from_resource (widget_class, "/me/hergert/Spiderweb/spiderweb-window.ui");
  gtk_widget_class_bind_template_child (widget_class, SpiderwebWindow, header_bar);
  gtk_widget_class_bind_template_child (widget_class, SpiderwebWindow, tab_view);
  gtk_widget_class_bind_template_child (widget_class, SpiderwebWindow, url_entry);
  gtk_widget_class_bind_template_callback (widget_class, on_url_entry_activate_cb);

  gtk_widget_class_install_action (widget_class, "tab.new", NULL, new_tab_action);
  gtk_widget_class_install_action (widget_class, "tab.close", NULL, close_tab_action);
  gtk_widget_class_install_action (widget_class, "url.focus", NULL, focus_url_action);

  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_t, GDK_CONTROL_MASK, "tab.new", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_w, GDK_CONTROL_MASK, "tab.close", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_l, GDK_CONTROL_MASK, "url.focus", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Left, GDK_ALT_MASK, "win.go-backward", NULL);
  gtk_widget_class_add_binding_action (widget_class, GDK_KEY_Right, GDK_ALT_MASK, "win.go-forward", NULL);

  g_type_ensure (SPIDERWEB_TYPE_TAB);
}

static void
spiderweb_window_init (SpiderwebWindow *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));

#ifdef DEVELOPMENT_BUILD
  gtk_widget_add_css_class (GTK_WIDGET (self), "devel");
#endif

  g_action_map_add_action_entries (G_ACTION_MAP (self),
                                   history_actions,
                                   G_N_ELEMENTS (history_actions),
                                   self);

  self->tab_bindings = g_binding_group_new ();
  g_binding_group_bind (self->tab_bindings,
                        "can-go-previous",
                        g_action_map_lookup_action (G_ACTION_MAP (self), "go-backward"),
                        "enabled",
                        G_BINDING_SYNC_CREATE);
  g_binding_group_bind (self->tab_bindings,
                        "can-go-next",
                        g_action_map_lookup_action (G_ACTION_MAP (self), "go-forward"),
                        "enabled",
                        G_BINDING_SYNC_CREATE);

  g_object_bind_property_full (self->tab_view, "selected-page",
                               self->tab_bindings, "source",
                               G_BINDING_SYNC_CREATE,
                               tab_page_to_tab, NULL, NULL, NULL);
}

void
spiderweb_window_append_tab (SpiderwebWindow *self,
                             SpiderwebTab    *tab)
{
  AdwTabPage *page;

  g_return_if_fail (SPIDERWEB_IS_WINDOW (self));
  g_return_if_fail (SPIDERWEB_IS_TAB (tab));

  page = adw_tab_view_append (self->tab_view, GTK_WIDGET (tab));
}

SpiderwebTab *
spiderweb_window_get_selected_tab (SpiderwebWindow *self)
{
  AdwTabPage *page;

  g_return_val_if_fail (SPIDERWEB_IS_WINDOW (self), NULL);

  if (!(page = adw_tab_view_get_selected_page (self->tab_view)))
    return NULL;

  return SPIDERWEB_TAB (adw_tab_page_get_child (page));
}

void
spiderweb_window_set_selected_tab (SpiderwebWindow *self,
                                   SpiderwebTab    *tab)
{
  AdwTabPage *page;

  g_return_if_fail (SPIDERWEB_IS_WINDOW (self));
  g_return_if_fail (!tab || SPIDERWEB_IS_TAB (tab));

  if (tab == NULL)
    return;

  if ((page = adw_tab_view_get_page (self->tab_view, GTK_WIDGET (tab))))
    adw_tab_view_set_selected_page (self->tab_view, page);
}

void
spiderweb_window_close_tab (SpiderwebWindow *self,
                            SpiderwebTab    *tab)
{
  AdwTabPage *page;

  g_return_if_fail (SPIDERWEB_IS_WINDOW (self));
  g_return_if_fail (!tab || SPIDERWEB_IS_TAB (tab));

  if (tab == NULL)
    return;

  if ((page = adw_tab_view_get_page (self->tab_view, GTK_WIDGET (tab))))
    adw_tab_view_close_page (self->tab_view, page);
}
