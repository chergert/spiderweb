/* spiderweb-application.h
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <adwaita.h>
#include <webkit2/webkit2.h>

G_BEGIN_DECLS

#define SPIDERWEB_TYPE_APPLICATION (spiderweb_application_get_type())

G_DECLARE_FINAL_TYPE (SpiderwebApplication, spiderweb_application, SPIDERWEB, APPLICATION, AdwApplication)

SpiderwebApplication *spiderweb_application_new                   (const char           *application_id,
                                                                   GApplicationFlags     flags);
gboolean              spiderweb_application_get_enable_javascript (SpiderwebApplication *self);
void                  spiderweb_application_set_enable_javascript (SpiderwebApplication *self,
                                                                   gboolean              enable_javascript);
WebKitWebContext     *spiderweb_application_get_web_context       (SpiderwebApplication *self);
WebKitSettings       *spiderweb_application_get_web_settings      (SpiderwebApplication *self);

G_END_DECLS
