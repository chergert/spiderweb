/* spiderweb-tab.c
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#define G_LOG_DOMAIN "spiderweb-tab"

#include "config.h"

#include <webkit2/webkit2.h>

#include "spiderweb-application.h"
#include "spiderweb-tab.h"

struct _SpiderwebTab
{
  GtkWidget      parent_instance;
  GtkWidget     *box;
  WebKitWebView *web_view;
};

enum {
  PROP_0,
  PROP_CAN_GO_PREVIOUS,
  PROP_CAN_GO_NEXT,
  N_PROPS
};

G_DEFINE_FINAL_TYPE (SpiderwebTab, spiderweb_tab, GTK_TYPE_WIDGET)

static GParamSpec *properties [N_PROPS];

static gboolean
on_web_view_decide_policy_cb (SpiderwebTab             *self,
                              WebKitPolicyDecision     *decision,
                              WebKitPolicyDecisionType  decision_type,
                              WebKitWebView            *web_view)
{
  g_assert (SPIDERWEB_IS_TAB (self));
  g_assert (WEBKIT_IS_POLICY_DECISION (decision));
  g_assert (WEBKIT_IS_WEB_VIEW (web_view));

  switch (decision_type)
    {
    case WEBKIT_POLICY_DECISION_TYPE_NAVIGATION_ACTION:
    case WEBKIT_POLICY_DECISION_TYPE_NEW_WINDOW_ACTION:
    case WEBKIT_POLICY_DECISION_TYPE_RESPONSE:
      break;

    default:
      return FALSE;
    }

  webkit_policy_decision_use (decision);

  return TRUE;
}

static gboolean
spiderweb_tab_grab_focus (GtkWidget *widget)
{
  return gtk_widget_grab_focus (GTK_WIDGET (SPIDERWEB_TAB (widget)->web_view));
}

static void
on_back_forward_list_changed_cb (SpiderwebTab          *self,
                                 WebKitBackForwardList *list)
{
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_GO_NEXT]);
  g_object_notify_by_pspec (G_OBJECT (self), properties [PROP_CAN_GO_PREVIOUS]);
}

static void
spiderweb_tab_dispose (GObject *object)
{
  SpiderwebTab *self = (SpiderwebTab *)object;

  g_clear_pointer (&self->box, gtk_widget_unparent);

  G_OBJECT_CLASS (spiderweb_tab_parent_class)->dispose (object);
}

static void
spiderweb_tab_get_property (GObject    *object,
                            guint       prop_id,
                            GValue     *value,
                            GParamSpec *pspec)
{
  SpiderwebTab *self = SPIDERWEB_TAB (object);

  switch (prop_id)
    {
    case PROP_CAN_GO_PREVIOUS:
      g_value_set_boolean (value, spiderweb_tab_get_can_go_previous (self));
      break;

    case PROP_CAN_GO_NEXT:
      g_value_set_boolean (value, spiderweb_tab_get_can_go_next (self));
      break;

    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
    }
}

static void
spiderweb_tab_class_init (SpiderwebTabClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  object_class->dispose = spiderweb_tab_dispose;
  object_class->get_property = spiderweb_tab_get_property;

  widget_class->grab_focus = spiderweb_tab_grab_focus;

  properties [PROP_CAN_GO_PREVIOUS] =
    g_param_spec_boolean ("can-go-previous", NULL, NULL, FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  properties [PROP_CAN_GO_NEXT] =
    g_param_spec_boolean ("can-go-next", NULL, NULL, FALSE,
                          (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS));

  g_object_class_install_properties (object_class, N_PROPS, properties);

  gtk_widget_class_set_layout_manager_type (widget_class, GTK_TYPE_BIN_LAYOUT);
}

static void
spiderweb_tab_init (SpiderwebTab *self)
{
  SpiderwebApplication *app = SPIDERWEB_APPLICATION (g_application_get_default ());
  WebKitSettings *web_settings = spiderweb_application_get_web_settings (app);
  WebKitWebContext *web_context = spiderweb_application_get_web_context (app);
  WebKitBackForwardList *list;

  self->box = g_object_new (GTK_TYPE_BOX,
                            "orientation", GTK_ORIENTATION_VERTICAL,
                            NULL);
  gtk_widget_set_parent (self->box, GTK_WIDGET (self));

  self->web_view = g_object_new (WEBKIT_TYPE_WEB_VIEW,
                                 "settings", web_settings,
                                 "web-context", web_context,
                                 "vexpand", TRUE,
                                 NULL);
  g_signal_connect_object (self->web_view,
                           "decide-policy",
                           G_CALLBACK (on_web_view_decide_policy_cb),
                           self,
                           G_CONNECT_SWAPPED);
  gtk_box_append (GTK_BOX (self->box), GTK_WIDGET (self->web_view));

  list = webkit_web_view_get_back_forward_list (self->web_view);
  g_signal_connect_object (list,
                           "changed",
                           G_CALLBACK (on_back_forward_list_changed_cb),
                           self,
                           G_CONNECT_SWAPPED);
}

SpiderwebTab *
spiderweb_tab_new (void)
{
  return g_object_new (SPIDERWEB_TYPE_TAB, NULL);
}

gboolean
spiderweb_tab_get_can_go_previous (SpiderwebTab *self)
{
  g_return_val_if_fail (SPIDERWEB_IS_TAB (self), FALSE);

  return webkit_web_view_can_go_back (self->web_view);
}

gboolean
spiderweb_tab_get_can_go_next (SpiderwebTab *self)
{
  g_return_val_if_fail (SPIDERWEB_IS_TAB (self), FALSE);

  return webkit_web_view_can_go_forward (self->web_view);
}

void
spiderweb_tab_go_previous (SpiderwebTab *self)
{
  WebKitBackForwardList *list;
  WebKitBackForwardListItem *item;

  g_return_if_fail (SPIDERWEB_IS_TAB (self));

  list = webkit_web_view_get_back_forward_list (self->web_view);
  item = webkit_back_forward_list_get_back_item (list);

  g_return_if_fail (item != NULL);

  webkit_web_view_go_to_back_forward_list_item (self->web_view, item);
}

void
spiderweb_tab_go_next (SpiderwebTab *self)
{
  WebKitBackForwardList *list;
  WebKitBackForwardListItem *item;

  g_return_if_fail (SPIDERWEB_IS_TAB (self));

  list = webkit_web_view_get_back_forward_list (self->web_view);
  item = webkit_back_forward_list_get_forward_item (list);

  g_return_if_fail (item != NULL);

  webkit_web_view_go_to_back_forward_list_item (self->web_view, item);
}

void
spiderweb_tab_navigate_to (SpiderwebTab *self,
                           const char   *url)
{
  g_return_if_fail (SPIDERWEB_IS_TAB (self));
  g_return_if_fail (url != NULL);

  /* TODO: if @url isn't a URL/URI/etc, use a search provider */

  g_print ("Navigating to %s\n", url);

  webkit_web_view_load_uri (self->web_view, url);
}
