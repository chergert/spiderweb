/* spiderweb-preferences.c
 *
 * Copyright 2022 Christian Hergert <chergert@redhat.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include "config.h"

#include "spiderweb-preferences.h"

struct _SpiderwebPreferences
{
  AdwWindow parent_instance;
};

G_DEFINE_FINAL_TYPE (SpiderwebPreferences, spiderweb_preferences, ADW_TYPE_WINDOW)

SpiderwebPreferences *
spiderweb_preferences_new (SpiderwebApplication *application)
{
  g_return_val_if_fail (SPIDERWEB_IS_APPLICATION (application), NULL);

  return g_object_new (SPIDERWEB_TYPE_PREFERENCES,
                       "application", application,
                       NULL);
}

static void
spiderweb_preferences_class_init (SpiderwebPreferencesClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/me/hergert/Spiderweb/spiderweb-preferences.ui");
}

static void
spiderweb_preferences_init (SpiderwebPreferences *self)
{
  gtk_widget_init_template (GTK_WIDGET (self));
}
